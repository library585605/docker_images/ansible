FROM librespace/ansible:8.4.0

RUN apt-get update && \
    apt-get install -y git curl jq  && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

CMD ["bash"]
